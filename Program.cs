﻿using System;

//Cuadrado
int FILA = 5, COLUMNA = 5;

for (int row = 0; row < FILA; row++)
{
    for(int col = 0; col < COLUMNA; col++)
    {
        Console.Write("*");
    }
    Console.Write("\n");
}

//Triangulo
Console.WriteLine();
for (int row = 0; row < FILA; row++)
{
    for(int col = 0;col < COLUMNA; col++)
    {
        if (col < row)
        Console.Write("*");
    }
    Console.Write("\n");
}

//Rombo
Console.WriteLine();
for (int row = 0; row < FILA; row++)
{
    for (int col = 0; col < COLUMNA; col++)
    {
        if (col < Math.Abs(FILA / 2 - row) || col >= COLUMNA - Math.Abs(FILA / 2 - row))
            Console.Write(" ");
        else
            Console.Write("*");
    }
    Console.WriteLine();
}

/*Console.WriteLine();
int t;

Console.Write("Ingrese el tamaño de los lados del rombo: ");
t = int.Parse(Console.ReadLine());

for (int f = -(t - 1); f < t; f++)
{
    for (int e = 1; e <= Math.Abs(f); e++)
        Console.Write(" ");

    for (int a = 1; a <= t - Math.Abs(f); a++)
        Console.Write("* ");

    Console.WriteLine();
}*/